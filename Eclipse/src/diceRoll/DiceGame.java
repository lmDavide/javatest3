// David Tran 1938381

package diceRoll;

import java.util.Random;

public class DiceGame {
	private Random diceRoll;
	private String message;
	private int outcome;
	
	public void startGame() {
		Random rand = new Random();
		this.diceRoll = rand;
	}
	
	public void playRound(String choice) {
		int diceNum = (this.diceRoll.nextInt(6) + 1); // (0 to 5) + 1 => (1 to 6)
		
		if(choice.equals("small")) {
			if(diceNum >= 1 && diceNum <= 3) {
				this.message = "You won!";
				this.outcome = 1;
			}
			else {
				this.message = "You lost.";
				this.outcome = -1;
			}
		}
		else if(choice.equals("large")) {
			if(diceNum >= 4 && diceNum <= 6) {
				this.message = "You won!";
				this.outcome = 1;
			}
			else {
				this.message = "You lost.";
				this.outcome = -1;
			}
		}
		else {
			this.message = "There was an error.";
			this.outcome = 0;
		}
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public int getOutcome() {
		return this.outcome;
	}
}
