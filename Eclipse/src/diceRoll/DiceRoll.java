package diceRoll;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class DiceRoll implements EventHandler<ActionEvent>{
	private TextField money;
	private TextField display;
	private TextField bet;
	private String choice;
	private DiceGame currentGame;

	public DiceRoll(TextField balance, TextField message, TextField playerBet, String playerChoice, DiceGame game) {
		this.money = balance;
		this.display = message;
		this.bet = playerBet;
		this.choice = playerChoice;
		this.currentGame = game;
	}
	
	public void handle(ActionEvent e) {
		if (Integer.parseInt(this.money.getText()) > Integer.parseInt(this.bet.getText())) {
			this.currentGame.playRound(this.choice);
			this.display.setText(this.currentGame.getMessage());
			this.money.setText( Integer.parseInt(this.money.getText()) + Integer.parseInt(this.bet.getText()) * this.currentGame.getOutcome() + "" );
		}
		else {
			this.display.setText("You do not have enough money.");
		}
	}

}
