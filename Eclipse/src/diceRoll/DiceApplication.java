//David Tran 1938381

package diceRoll;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class DiceApplication extends Application {
	private DiceGame currentGame;
	
	public void start(Stage stage) {
		DiceGame newGame = new DiceGame();
		newGame.startGame();
		this.currentGame = newGame;
		
		Group root = new Group(); 
		
		/**
		 * Adding Containers and its content
		 */
		TextField moneyInfo = new TextField("Money:");
		TextField money = new TextField("500");
		HBox moneyHbox = new HBox();
		moneyHbox.getChildren().addAll(moneyInfo, money);
		
		TextField betInfo = new TextField("Bet:");
		TextField bet = new TextField();
		HBox betBox = new HBox();
		betBox.getChildren().addAll(betInfo, bet);
		
		Button smallButton = new Button("Small");
		Button largeButton = new Button("Large");
		HBox buttonsBox = new HBox();
		buttonsBox.getChildren().addAll(smallButton, largeButton);
		
		TextField message = new TextField("Welcome to the dice roll game!");
		message.setMinWidth(300);
		HBox messageBox = new HBox();
		messageBox.getChildren().addAll(message);
		
		VBox newVbox = new VBox();
		newVbox.getChildren().addAll(moneyHbox, betBox, buttonsBox, messageBox);
		root.getChildren().add(newVbox);

		/**
		 * Adding EventHandler to the buttons
		 */
		DiceRoll diceRollSmall = new DiceRoll(money, message, bet, "small", this.currentGame);
		DiceRoll diceRollLarge = new DiceRoll(money, message, bet, "large", this.currentGame);
		
		smallButton.setOnAction(diceRollSmall);
		largeButton.setOnAction(diceRollLarge);
		
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Dice Roll Game"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
 public static void main(String[] args) {
     Application.launch(args);
     
 }
}   