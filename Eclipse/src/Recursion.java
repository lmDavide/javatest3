// David Tran 1938381

public class Recursion {
	public static void main(String[]args) {
		int[] numbers = {40, 40, 0};
		System.out.println(recursiveCount(numbers, 1));
	}
	
	// ???
	// Brain.exe has stopped working :D
	public static int recursiveCount(int[] numbers, int n){
		int count = 0;
		
		if(n > numbers.length) {
			return count;
		}
		
		
		if(numbers[n] > 20 && (n % 2 == 1)) {
			count++;
		}
		
		recursiveCount(numbers, n+1);
		
		return count;
	}
	
}